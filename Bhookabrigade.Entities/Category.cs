﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bhookabrigade.Entities
{
	public class Category : BaseEntity
	{
		public List<Products> Products { get; set; }
	}
}
