﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Bhookabrigade.web.Startup))]
namespace Bhookabrigade.web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
